var express = require('express');
//var request = require('request');
var app = express();

// set the directory where express is going to look for templates
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/public'));
app.engine('html', require('ejs').renderFile);

// get json from curl/api call
app.use(express.bodyParser());

// start an http server on port 3031
var server = app.listen(3031);
var io = require('socket.io').listen(server);

// Set credentials for the admin map page
var auth = express.basicAuth(function(user, pass) {     
   return (user == "neo4j" && pass == "mac^6iris");
},'Map Admin Area');

// set what will occur on a GET request at the '/' route
app.get('/', auth, function(req, res) {
	res.render('index.html');
});

// app.post('/marker', function(req, res) {
// 	console.log(req.body);
// 	io.sockets.broadcast.emit('update', req.body);
// 	res.setHeader('content-type', 'application/json');
// 	res.send('{"status" : "success"}\n');
// });

// // api for updating data on main dashboard page
// app.post('/update', function(req, res) {
// 	console.log(req.body);
// 	io.sockets.emit('update', req.body);
// 	res.setHeader('content-type', 'json/application');
// 	res.send('{"status" : "success"}\n');
// });

// app.post('/status', function(req, res) {
// 	console.log(req.body);
// 	io.sockets.emit('status', req.body);
// 	res.setHeader('content-type', 'json/application');
// 	res.send('{"status" : "success"}\n');
// });

// io.sockets.on('marker', function(socket) {

// });

//start listening for connections socket.io
io.sockets.on('connection', function(socket) {
	socket.on('marker', function (data) {
    	socket.broadcast.emit('update', data);
  	});
  	socket.on('relationship', function (data){
  		socket.broadcast.emit('relationship', data);
  	})
});
