# README #

# What is this repository for? #

* This is the node.js server that is used for the admin portal where users can add/edit the map database.

# What files should I edit/improve on this repo? #

* When the VM is instantiated you will need to edit the serverURL variable in /view/index.html
* The page that is loaded by going the the url mentioned below is located at: /views/index.html
* The node.js server settings (including the credentials to login to the admin page) are in: /admin.js
* The resources used in /views/index.html are located in: /public

# How do I access the web page hosted by this node.js server? #

* Server url: http://<url_of_VM_server>:3031   
* The username is: neo4j  
* The password is: mac^6iris
* Note: these credentials/port are set in the /admin.js file

# How do I get set up? #

* This repo is already loaded on the VM server that is included in the project.  The node.js server is setup to automatically start itself when the vm is booted.  If the server needs to be started again for some reason, it can be done either by restarting the server or by using the commands below.

# Where is this repo located on the VM server? #

* It is located at ~/admin

### Commands: ###
* start server (be in the ~ directory to run this command. Get there by doing: cd ~): nohup nodejs admin/admin.js &
* Check if node script is running: ps -ef | grep nodejs
* Kill server by doing: kill -9 <PID found using the ps -ef command above>

# Who do I talk to if I have questions about this? #

* Jeff Wenzbauer: jwenz723@gmail.com
* Klint Holmes: klintholmes@weber.edu